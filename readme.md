# Launch mongodb and redis

```
docker-compose mongo redis
```

# Launch server and web frontend

All services can be launched in docker.

To run on local computer:

```
./serve server
./serve client
./serve chatterbot
./
```

