import React from 'react';
import { Avatar as MAvatar } from 'material-ui';

class Avatar extends React.Component {
  render() {
    return (<MAvatar src={this.props.user.avatar} />)
  }
}

module.exports = Avatar;
