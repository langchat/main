const hostname = window && window.location && window.location.hostname;
//const hostname = "test"

const API_ROOT = (hostname === 'localhost') ? 'http://localhost:3001/api' : 'http://langchat.me/api';
const SOCKET_URL = (hostname === 'localhost') ? 'http://localhost:3001' : 'http://langchat.me';

module.exports = { API_ROOT, SOCKET_URL }
