import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';
import io from 'socket.io-client'

const superagent = superagentPromise(_superagent, global.Promise);
const { API_ROOT, SOCKET_URL } = require('./api-config');
console.log(API_ROOT)
const encode = encodeURIComponent;
const responseBody = res => res.body;

let token = null;
var socket = null;
const tokenPlugin = req => {
  if (token) {
    req.set('authorization', `Token ${token}`);
  }
}

const requests = {
  del: url =>
    superagent.del(`${API_ROOT}${url}`).use(tokenPlugin).then(responseBody),
  get: url =>
    superagent.get(`${API_ROOT}${url}`).use(tokenPlugin).then(responseBody),
  put: (url, body) =>
    superagent.put(`${API_ROOT}${url}`, body).use(tokenPlugin).then(responseBody),
  post: (url, body) =>
    superagent.post(`${API_ROOT}${url}`, body).use(tokenPlugin).then(responseBody),
  postFile: (url, file, body) =>
    superagent.post(`${API_ROOT}${url}`, body).use(tokenPlugin).attach('file', file).then(responseBody),
};

const Socket = {
  emit: (event, body) => {
    if (socket) socket.emit(event, Object.assign(body, { token }))
  },
  on: (event, callback) => {
    if (socket) socket.on(event, callback)
  },
}

const Auth = {
  current: () => requests.get('/me'),
  login: (email, password) => requests.post('/login', { email, password }),
  register: (username, email, password) =>
    requests.post('/users', { user: { username, email, password } }),
  save: user => requests.put('/me', { user }),
  loginFacebook: token => requests.get(`/auth/facebook?token=${token}`),
};

const limit = (count, p) => `limit=${count}&offset=${p ? p * count : 0}`;
const omitSlug = article => Object.assign({}, article, { slug: undefined });

const Friends = {
  all: () =>
    requests.get(`/friends`),
  unfriend: id =>
    requests.del(`/friends/${id}`),
  get: id =>
    requests.get(`/friends/${id}`),
  blocked: () =>
    requests.del(`/friends?blocked=true`),
  add: id =>
    requests.post('/friends', { id })
};

const Threads = {
  all: () => requests.get('/threads'),
  add: (tid, msg) => requests.post(`/thread/`, { msg }),
  delete: (tid, msgId) => requests.del(`/thread/${tid}/messages/${msgId}`),
  messages: tid => requests.get(`/thread/${tid}/messages`),
  startRequest: () => Socket.emit('/thread-request/start', { }),
  stopRequest: () => Socket.emit('/thread-request/stop', { }),
  onNewRequest: callback => Socket.on('/thread-request/new', callback),
  getByUser: user => requests.get(`/thread?uid=${user.id}`),
};

const Messages = {
  send: (threadId, content) => {
    console.log('socket'); console.log(socket)
    return Socket.emit('add_message', { threadId, content }); },
  //requests.post(`/thread/${tid}/message`, { content }),
  delete: (threadId, msgId) =>
    requests.del(`/thread/${threadId}/messages/${msgId}`),
  all: threadId =>
    requests.get(`/thread/${threadId}/messages`),
  onNewMsg: (tid, callback) => Socket.on('message_added', (msg) => {
    console.log(msg)
    console.log(tid)
    if (tid === msg.threadId) callback(msg)
  })
};

const Profile = {
  me: () => requests.get('/me'),
  follow: username =>
    requests.post(`/profiles/${username}/follow`),
  get: username =>
    requests.get(`/user?username=${username}`),
  unfollow: username =>
    requests.del(`/profiles/${username}/follow`),
  setAvatar: (file) => requests.postFile('/me/avatar', file),
};

module.exports = {
  Auth,
  Friends,
  Threads,
  Messages,
  Profile,
  setToken: _token => {
    token = _token;
    socket = io(SOCKET_URL, { query: { token } });
    socket.on('connect', () => {
      console.log('connected')
    })
    socket.on('disconnect', () => {
      console.log('disconnected')
    })
  },
  logout: () => {
    token = null
    socket = null
  }
};
