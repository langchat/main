from scrapy.selector import Selector
from scrapy.http import HtmlResponse
import scrapy, json, random, re
import logging
import os
from datetime import date
import base64
import hashlib

class NewsSpider(scrapy.Spider):
    name = 'news'
    def start_requests(self):
        yield scrapy.Request(url='https://headlines.yahoo.co.jp/hl?a=20180130-00010004-huffpost-int', callback=self.parse)
    
    def parse(self, response):
        #page = response.url.split("/")[-2]
        obj = {}
        obj['url'] = response.url
        obj['title'] = response.xpath('//h1/text()').extract_first()
        obj['content'] = response.xpath('''//p[@class='ynDetailText']''').extract_first()
        obj['date'] = response.xpath('''//p[@class='source']/text()''').extract_first()
        
        regex = r"(\d{1,2})/(\d{1,2})"
        matches = re.findall(regex, obj['date'])

        if len(matches) > 0:
            m, d = matches[0]
        else: m, d = 0, 0
        y = 2018
        obj['date'] = date(y, int(m), int(d)).isoformat()

        dir_name = os.path.join(os.path.dirname(__file__), "..", "..", "data", 
                obj['date'], 'yahoo')
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)
        f = open(os.path.join(dir_name, hashlib.md5(obj['url'].encode()).hexdigest() + '.json'), "w")
        f.writelines(json.dumps(obj, indent=2))
        f.close()
        