import React from 'react';
import { Grid, Paper, IconButton } from 'material-ui';
import GridList, { GridListTile, GridListTileBar } from 'material-ui/GridList';
import Subheader from 'material-ui/List/ListSubheader';
import { Info } from 'material-ui-icons';
import { agent } from 'langchat-client'
import sources from './sources'
//const mapStateToProps = state => ({
//  ...state.home,
//  appName: state.common.appName,
//  token: state.common.token,
//});

//const mapDispatchToProps = dispatch => ({
  
//});

const styles = {
  root: {
    height: '100%',
  },
}

class ReadingChallenge extends React.Component {
  componentWillMount() {
    //this.props.onLoad(Promise.all([agent.Friends.all(), agent.Threads.all()]));
  }

  componentWillUnmount() {
    //this.props.onUnload();
  }

  render() {
    return (
      <div style={styles.root}>
        <GridList cellHeight={180} cols={6}>
        <GridListTile key="Subheader" cols={6} style={{ height: 'auto' }}>
          <Subheader component="div">December</Subheader>
        </GridListTile>
        {sources.map(source => (
          <GridListTile key={source.id}>
            <img src={source.img} alt={source.title} />
            <GridListTileBar
              title={source.title}
              subtitle={<span>{source.homepage}</span>}
              actionIcon={
                <IconButton>
                  <Info />
                </IconButton>
              }
            />
          </GridListTile>
        ))}
      </GridList>
      </div>
    );
  }
}

//export default connect(mapStateToProps, mapDispatchToProps)(ReadingChallenge);
export default ReadingChallenge