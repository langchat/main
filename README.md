# Run

```
./serve db
./serve server
./serve web
./serve app
./serve chatterbot
```

# Binding Ports


| Service | Port  |
|---------|--------|
| MongoDB | 27017 |
| Redis | 6379 |
| Server | 3001 |
| Client (web) | 8080 |